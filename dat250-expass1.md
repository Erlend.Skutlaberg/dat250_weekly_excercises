# Erlend Skutlaberg

## Installation
I verified my java jdk by opening the terminal and executed the command "java --version", which verifyed that i have a java jdk on my computer.
I verified my IDE by opening Intellij on my computer.
I verified that i had Maven by executing the command "mvn --version" in terminal, which returned my current version of maven
I verified that i had a git client by executing the command "git --version", which verified that it was installed

## Heroku
I didn't really encounter any technical problems. I did however encounter a small problem where i forgot to change the string which 
was printed to the /hello page, so the value of energy didn't change from 12 to 20 GeV. 
